# Universidad Nacional de San Luis - Ing. Electrónica OSD

### Arquitectura de las Computadoras 2019
###### Desarrollo
En este trabajo práctico se diseño un datapath para un sub-conjunto del Set de Instrucciones de RISC-V.  Se realiza la implementación del procesador en VHDL y simulación en Cocotb de las siguentes instrucciones:

`add sub addi and or xor ld sd sll srl sra` 
`lui andi ori xori slli srli  srai` 
`beq bne blt bge jal jalr`


###### Esquema Datapath
La siguiente imagén es un esquema del datapath del procesador monociclo para la implementación de las instrucciones anteriores.
Se observa que las señales de control para los saltos condicionales estan dentro del mismo bloque de control. De esta manera no se modifico el bloque sumador ALU.


![](Datapath.jpg)


Aqui se encuentra un Link para libro excel donde muestra la tabla de verdad de señales de control del Datapath:


`<link>` : <https://gitlab.com/flaco.jrp/risc-v/blob/master/Tabla.xlsx>


###### Modificaciones en codigos
Codigos .vhd:

*  **UC.vhd**: se agregaron señales de saltos(reconoce sus opcodes), se modifica señal ALUop.
*  **Up.vhd**: se agregan señales de control, multiplexores y comparador.
*  **ProgMem.vhd**: se quita clock (procesador monociclo).
*  **ImmGen.vhd**: se hace diferencia entre el inmediato generado para las tipo I aritmetico/logicas y las de despĺazamiento.

Codigos .py

*  **ProgMem_tb.py**: se modifica para que el testeo sea el correcto.
*  **risc_v_tb.py**: se modifica para que muestre en pantalla varias señales con diferentes colores.

Codigos .txt
*  **Program.txt**: se modifica para que al testear se ejecuten todas las instrucciones requeridas.



###### Código de Prueba
Se puede observar el codigo de máquina simulado en Cocotb.

```
        addi x1,x0,0
	ori x1,x0,5	        #x1<---5
	addi x4,x0,11	        #X4<---11
	andi x5,x4,5
	xori x5,4
	slli x6,x5,3
	srli x6,x6,3
	srali x6,x5,2
	sd x6,7(x1)  
	ld x3,7(x1)
	add x3,x4,x3
	sub x3,x4,x1
	sll x3,x4,x1
	slr x3,x4,x1
	sra x3,x4,x1
	xor x3,x4,x1
	or x3,x4,x1
	and x3,x4,x1
	beq x4,x1,S0	        #no salta
	beq x1,x1,S0	        #salta a S0
	
S0:	bne x1,x1,S1	        #no salta
	bne x4,x1,S1	        #salta a S1
	
S1:	blt x4,x1,S2	        #no salta
	blt x1,x4,S2	        #salta a S2
	
S2:	bge x1,x4,S3	        #no salta
	bge x4,x1,S3	        #salta a S3

S3:	jal x2,S4	        #salta a S4


S4:	lui x4,1000
	jalr x2, 31(x1)	        #(31+x1=S5) salta a S5
    
S5: addi x1,x0,0

    
    Se completa con addi x1,x0,0 hasta la linea 1024
```


En el siguiente link se puede descargar un libro de excel donde se detallan las instrucciones con su Opcode, ademas de la ubicación del puntero PC:


`<link>` : <https://gitlab.com/flaco.jrp/risc-v/blob/master/opcodes.xlsx>


------------

##### Test
Para realizar el test es necesario tener instado Docker ejecutar en la terminal los siguentes comandos:


`..//docker/ ./dockershell`

luego

`..//test_python/FullRiscV/ make`

En la siguiente imagen se muestra la ejecución FullRiscV.py

![](Test.png)


------------

##### Conclusiones
Con los cambios que se realizaron en los archivos de codigo como asi tambien en los de test, se logro con exito la implementacion del sub-conjunto de set de instrucciones para el datapath diseñado.
De esta forma se realiza una mejora al procesador visto en el libro y se obtuvo experiencia en el diseño de procesadores.  
Quedan pendientes la correccion de algunos detalles que podrian mejorar el funcionamiento del mismo.

------------
##### Referencias
###### - https://content.riscv.org/wp-content/uploads/2017/05/riscv-spec-v2.2.pdf
###### - https://www.docker.com/  
###### - https://cocotb.readthedocs.io/en/latest/
------------


##### Integrantes

Agustin Quevedo

Jonathan Pelliciari


