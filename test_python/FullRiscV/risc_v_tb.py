import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure

CLK_PERIOD = 10

@cocotb.coroutine
def Reset(dut):
    dut.RST_i <=  0
    yield Timer(CLK_PERIOD * 1)
    dut.RST_i  <= 1
    yield Timer(CLK_PERIOD * 10)
    dut.RST_i  <= 0

@cocotb.test()
def test(dut):
    """
    Description:
        Test RISC-V Simple Implementation
    """
    cocotb.fork(Clock(dut.CLK_i, CLK_PERIOD).start())
    
    yield Reset(dut)

    #yield Timer(CLK_PERIOD * 64)

    for i in range(64):
        yield RisingEdge(dut.CLK_i)
        
        print('\033[2;37;40mSeñales de Datos')
        print('\033[1;32;40mProgramCounter:',dut.PCOUT.value.integer)
        print('\033[1;34;40mInstruccion')
        print(dut.Instruction.value)
        print('\033[1;31;40mInmediato')
        print(dut.ImmGenOUT.value)
        print('\033[1;33;40mDato de Registro 1')
        print(dut.ReadDATA1toALU.value)
        print('\033[1;33;40mDato de Registro 2')
        print(dut.ReadDATA2toALU.value)
        print('\033[1;36;40mValor de Salida de ALU')
        print(dut.ALUresult.value)
        print('\033[1;33;40mDato a Escribir en Registro')
        print(dut.MUX_used_to_J_type.value)
        print('\033[1;33;40mDato a Escribir en Memoria')
        print(dut.ReadDATA2toALU.value)
        print('\033[1;33;40mDato a Leer de Memoria')
        print(dut.DataMEMtoMux.value)
        print('\n')
        
        print('\033[2;37;40mSeñales de Control')
        if dut.ALUOp.value.integer == 0:
            print('\033[1;35;40mOperacion de ALU: Suma')
        elif dut.ALUOp.value.integer == 1:
            print('\033[1;35;40mOperacion de ALU: Resta')
        elif dut.ALUOp.value.integer == 3:
            print('\033[1;35;40mOperacion de ALU: AND')
        elif dut.ALUOp.value.integer == 4:
            print('\033[1;35;40mOperacion de ALU: OR')
        elif dut.ALUOp.value.integer == 5:
            print('\033[1;35;40mOperacion de ALU: XOR')
        elif dut.ALUOp.value.integer == 6:
            print('\033[1;35;40mOperacion de ALU: Desplazamiento Derecha R')
        elif dut.ALUOp.value.integer == 7:
            print('\033[1;35;40mOperacion de ALU: Desplazamiento Derecha I')
        elif dut.ALUOp.value.integer == 8:
            print('\033[1;35;40mOperacion de ALU: Desplazamiento Izquierda R')
        elif dut.ALUOp.value.integer == 11:
            print('\033[1;35;40mOperacion de ALU: Desplazamiento Izquierda I')
        else:
            print('\033[1;35;40mOperacion de ALU: Sin Operación')
        print('\033[1;35;40mSeñal de Control RegWrite:',dut.RegWrite.value.integer)
        print('\033[1;35;40mSeñal de Control Sel:',dut.Sel.value.integer)
        print('\033[1;35;40mSeñal de Control Jalr:',dut.Jalr.value.integer)
        print('\033[1;35;40mSeñal de Control MemWrite:',dut.MemWrite.value.integer)
        print('\033[1;35;40mSeñal de Control MemRead:',dut.MemRead.value.integer)
        print('\033[1;35;40mSeñal de Control ALUSrc:',dut.ALUSrc.value.integer)
        print('\n')

            

